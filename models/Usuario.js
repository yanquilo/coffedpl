let mongoose = require("mongoose");

let Schema = mongoose.Schema;

let bcrypt = require("bcrypt");

let crypto = require("crypto");

let Token = require("./Token");

let uniqueValidator = require("mongoose-unique-validator");

let Encargar = require("./Encargar");

let mailer = require("../mailer/mailer");

let saltRounds = 10;

//Verificar el formato del correo
let validateEmail = function (email) {

    let re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;

    return re.test(email);

}

let usuarioSchema = new Schema({

    nombre: {

        type: String,

        trim: true,

        required: [true, "El nombre es obligatorio"]

    },

    email: {

        type: String,

        trim: true,

        required: [true, "El email es obligatorio"],

        lowercase: true,

        unique: true,

        validate: [validateEmail, "Por favor, introduzca un email válido"],

        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/]

    },

    password: {

        type: String,

        required: [true, "El password es obligatorio"]

    },

    passwordResetToken: String,

    passwordResetTokenExpires: Date,

    verificado: {

        type: Boolean,

        default: false

    }

});

//Encriptar password
usuarioSchema.pre("save", function (next) {

    if (this.isModified("password")) {

        this.password = bcrypt.hashSync(this.password, saltRounds);

    }

    next();

});

//Encargar
usuarioSchema.methods.encargar = function (coffeId, desde, cb) {

    let from = new Date(desde);

    let encargar = new Encargar({ usuario: this._id, coffe: coffeId, desde: from});

    console.log(encargar);

    encargar.save(cb);

};

//Create
usuarioSchema.statics.add = function (aUser, cb) {
    return this.create(aUser, cb);
};

//Find
usuarioSchema.statics.allUsers = function (cb) {
    return this.find({}, cb);
};

//Para poder usar el mongoose-unique-validator
usuarioSchema.plugin(uniqueValidator, { message: "El email ya existe con otro usuario." });

//Validar el password
usuarioSchema.methods.validPassword = function (password) {

    return bcrypt.compareSync(password, this.password);

}

//Email de confimación
usuarioSchema.methods.enviar_email_bienvenida = function (cb) {

    let token = new Token({ _userId: this.id, token: crypto.randomBytes(16).toString("hex") }); //El token es un String en hexadecimal

    let email_destination = this.email;

    token.save(function (err) {

        if (err) { return console.log(err.message); }



        let mailOptions = {

            from: "no-reply@greatCoffe.com",

            to: email_destination,

            subject: "Verificación de cuenta",

            text: "Hola,\n\n" + "Por favor, para verificar su cuenta haga click en este enlace: \n" + "http://192.168.56.103:3000" + "\/token/confirmation\/" + token.token + ".\n"

        };



        mailer.sendMail(mailOptions, function (err) {

            if (err) { return console.log(err.message); }

            console.log("Se ha enviado un email de bienvenida a " + email_destination + ".");

        });

    });

};


module.exports = mongoose.model("Usuario", usuarioSchema);