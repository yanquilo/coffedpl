let mongoose = require("mongoose");

let Schema = mongoose.Schema;

let coffeSchema = new Schema({
    coffeID: Number,
    tipo: String,
    cantidad: String,
    claridad: String
});
//Find
coffeSchema.statics.allCoffe = function (cb) {
    return this.find({}, cb);
};
//Create
coffeSchema.statics.add = function (aCoffe, cb) {
    return this.create(aCoffe, cb);
};
//FinById
coffeSchema.statics.coffeId = function (aCoffeId, cb) {
    return this.findById(aCoffeId, cb);
};
//RemoveById
coffeSchema.statics.removeById = function (aCoffeId, cb) {
    return this.findByIdAndRemove(aCoffeId, cb);
};
//UpdateById
coffeSchema.statics.updateById = function(aCoffeId, coffe, cb){
    return this.findByIdAndUpdate(aCoffeId, coffe, cb);
};

module.exports = mongoose.model("Coffe", coffeSchema);