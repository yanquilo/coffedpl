let mongoose = require("mongoose");

let Schema = mongoose.Schema;

let encargarSchema  = new Schema ({

    desde: Date,

    coffe: {type: mongoose.Schema.Types.ObjectId, ref: "Coffe"},

    usuario: {type: mongoose.Schema.Types.ObjectId, ref: "Usuario"}

});

//desde cuando se hizo el encargo del café

encargarSchema.methods.diaDeEncargo = function () {

    return moment(this.hasta.diff(moment(this.desde), "days") + 1);

};

//Find
encargarSchema.statics.allReserv = function (cb) {
    return this.find({}, cb);
};

module.exports = mongoose.model("Encargar", encargarSchema);