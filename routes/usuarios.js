let express = require('express');

let router = express.Router();

let usuariosController = require("../controllers/usuario");



router.get("/", usuariosController.list);



router.get("/create", usuariosController.create_get);

router.post("/create", usuariosController.create);



router.post("/:id/delete", usuariosController.delete);



router.get("/:id/update", usuariosController.update_get);

router.post("/:id/update", usuariosController.update);




module.exports = router;