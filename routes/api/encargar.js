//Creación de la ruta para la APIController

let express = require('express');

let router = express.Router();

let encargarControllerAPI = require("../../controllers/api/encargarControllerAPI");

//mostrar
router.get("/", encargarControllerAPI.encargar_list);

module.exports = router;