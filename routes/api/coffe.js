//Creación de la ruta para la APIController

let express = require('express');

let router = express.Router();

let coffeControllerAPI = require("../../controllers/api/coffeControllerAPI");

//mostrar
router.get("/", coffeControllerAPI.coffe_list);

//creacion
router.post("/create", coffeControllerAPI.coffe_create);

//borrado
router.delete("/delete", coffeControllerAPI.coffe_delete);

//actualizar
router.put("/update", coffeControllerAPI.coffe_update);


module.exports = router;