//Creación de la ruta para la APIController

let express = require('express');

let router = express.Router();

let usuariosControllerAPI = require("../../controllers/api/usuariosControllerAPI");

//Encargar
router.post("/encargar", usuariosControllerAPI.usuarios_encargo);

//mostrar
router.get("/", usuariosControllerAPI.usuario_list);

//creacion
router.post("/create", usuariosControllerAPI.usuarios_create);

module.exports = router;