var express = require('express');

var router = express.Router();

let coffeController = require('../controllers/coffe');

router.get("/", coffeController.coffe_list);
router.get("/create", coffeController.coffe_create_get);
router.post("/create", coffeController.coffe_create_post);
router.post("/:id/delete", coffeController.coffe_delete_post);
router.get("/:id/update", coffeController.coffe_update_get);
router.post("/:id/update", coffeController.coffe_update_post);

module.exports = router;