let Coffe = require("../models/Coffe");

exports.coffe_list = function (req, res) {
    Coffe.allCoffe(function (err, cafes) {
        if (err) res.status(500).send(err.message);
        res.render("coffe/index", {
            cafes: cafes
        })
    })
}

exports.coffe_create_get = function (req, res) {
    res.render("coffe/create");
}

exports.coffe_create_post = function (req, res) {
    let cafe = new Coffe({
        coffeID: req.body.coffeID,
        tipo: req.body.tipo,
        cantidad: req.body.cantidad,
        claridad: req.body.claridad
    });

    Coffe.add(cafe, function (err, cafes) {
        if (err) res.status(500).send(err.message);
        res.redirect("/coffe")
    })
}

exports.coffe_delete_post = function (req, res) {
    Coffe.removeById(req.body.id, function (err, cafes) {
        if (err) res.status(500).send(err.message);
        res.redirect("/coffe")
    })
}

exports.coffe_update_get = function (req, res) {
    console.log(req.body.id);
    Coffe.findById(req.params.id, function (err, cafe) {
        if (err) res.status(500).send(err.message);
        res.render("coffe/update", { cafe });
    })

}

exports.coffe_update_post = function (req, res) {
    Coffe.updateById(req.body.id, req.body, function (err, cafes, _id) {
        if (err) res.status(500).send(err.message);
        res.redirect("/coffe")
    })
}
