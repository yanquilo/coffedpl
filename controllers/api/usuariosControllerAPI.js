let Usuario = require("../../models/Usuario");

//Controlador

//reservar
exports.usuarios_encargo = function (req, res) {

    Usuario.findById(req.body._id, function (err, usuario) {

        console.log(req.body);
        if (err) res.status(500).send(err.message);

        console.log(usuario);

        usuario.reservar(req.body.bici_id, req.body.desde, req.body.hasta, function (err) {

            console.log("Reservada!!");

            res.status(200).send();

        });

    });

};

//List
exports.usuario_list = function (req, res) {
    Usuario.allUsers(function (err, users) {
        if (err) res.status(500).send(err.message);
        res.status(200).json({
            usuarios: users
        });
    });
};

//Create
exports.usuarios_create = function (req, res) {

    let user = new Usuario({
        nombre: req.body.nombre,
    });

    Usuario.add(user, function (err, newUser) {
        if (err) res.status(500).send(err.message);
        res.status(201).send(newUser);
    });
};