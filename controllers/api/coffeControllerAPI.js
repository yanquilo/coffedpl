let Coffe = require("../../models/Coffe");

//Controlador de la API

exports.coffe_list = function (req, res) {
    Coffe.allCoffe(function (err, cafes) {
        if (err) res.status(500).send(err.message);
        res.status(200).json({
            coffes: cafes
        });
    });
};

exports.coffe_create = function (req, res) {

    let cafe = new Coffe({
        coffeID: req.body.coffeID,
        tipo: req.body.tipo,
        cantidad: req.body.cantidad,
        claridad: req.body.claridad
    });

    Coffe.add(cafe, function (err, newCoffe) {
        if (err) res.status(500).send(err.message);
        res.status(201).send(newCoffe);
    });
};

exports.coffe_delete = function (req, res) {
    Coffe.removeById(req.body.id, function (err, cafes) {
        if (err) res.status(500).send(err.message);
        res.status(204).send();
    })
}

exports.coffe_update = function (req, res) {
    Coffe.updateById(req.body.id, req.body, function (err, cafes) {
        if (err) res.status(500).send(err.message);
        res.status(202).send(cafes);

    })

}