let Encargar = require("../../models/Encargar");

//Controlador

//List
exports.encargar_list = function (req, res) {
    Encargar.allEncarg(function (err, encarg) {
        if (err) res.status(500).send(err.message);
        res.status(200).json({
            encargos: encarg
        });
    });
};