var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

//Conexion
var mongoose = require('mongoose');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var coffeRouter = require('./routes/coffe');

//Variable del router de la API
var coffeAPIRouter = require('./routes/api/coffe');
var usuariosAPIRouter = require("./routes/api/usuarios");
var encargarAPIRouter = require("./routes/api/encargar");
var usuariosRouter = require("./routes/usuarios");
var tokenRouter = require("./routes/tokens");

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/coffe', coffeRouter);
app.use('/api/coffe', coffeAPIRouter);
app.use('/api/usuarios', usuariosAPIRouter);
app.use('/api/encargar', encargarAPIRouter);
app.use("/token", tokenRouter);
app.use("/usuarios", usuariosRouter);

//Conexion
mongoose.connect('mongodb://localhost/greatCoffe', { useUnifiedTopology: true, useNewUrlParser: true });

mongoose.Promise = global.Promise;

var db = mongoose.connection;

//Bind connection to error event (to get notification of conection errors)

db.on("error", console.error.bind('Error de conexión con MongoDB'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
